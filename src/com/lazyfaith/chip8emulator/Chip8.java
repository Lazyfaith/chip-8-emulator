package com.lazyfaith.chip8emulator;


/*
    Main reference material:

    http://devernay.free.fr/hacks/chip8/C8TECH10.HTM    (later found to contain a few errors)
    https://en.wikipedia.org/wiki/CHIP-8
    http://mattmik.com/chip8.html                       (best reference doc, fixed errors of the first)

 */

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Chip8 {

    // RAM
    byte[] memory = new byte[4096];

    // Registers
    int[] v = new int[16]; // 16 registers named V0 through VF (should be single byte each but using int for easier calculations)
    short i = 0x0000; // 16 bit address register
    byte delayTimer = 0x00; // 8 bit special purpose register, decrements at 60Hz (i.e. if it's at 60 then it will reach 0 in exactly 1 second)
    byte soundTimer = 0x00; // 8 bit special purpose register, decrements at 60Hz. Sound plays when this is not 0

    short pc = 0x0200; // 16 bit program counter, "used to store the currently executing address". Most programs start at 0x200
    byte sp = 0x00; // 8 bit stack pointer, "it is used to point to the topmost level of the stack" (in this implementation, the first element that's free)
    short[] stack = new short[16]; // "16 16-bit values, used to store the address that the interpreter shoud return to when finished with a subroutine"

    // VM components
    Screen screen = null;
    Keypad keypad = null;

    // Tick/cycle related vars
    boolean cleanStart = true;
    boolean romLoaded = false;
    boolean running = false;
    long cycles = 0;
    long lastCpuTick = 0;
    long lastTimerTick = 0;

    // Whether each memory address is locked at it's current value or not
    boolean[] memoryLocked = new boolean[4096];

    public Chip8() {
        // Create keypad obj
        keypad = new Keypad();

        // Prepare screen & fill with starting splash
        screen = new Screen();

        // Set emulator to clean start
        hardReset();
    }

    public void hardReset() {
        memory = new byte[4096];
        loadFont();

        // Registers
        v = new int[16];
        i = 0x0000;
        delayTimer = 0x00;
        soundTimer = 0x00;
        pc = 0x0200;
        sp = 0x00;
        stack = new short[16];

        // Tick/cycle related vars
        cleanStart = true;
        romLoaded = false;
        running = false;
        cycles = 0;
        lastCpuTick = 0;
        lastTimerTick = 0;

        displaySplashScreen();
    }

    private void loadFont() {
        // An array of values to be stored in memory as sprites for displaying hex characters
        int [] hexDigits = {
                0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
                0x20, 0x60, 0x20, 0x20, 0x70, // 1
                0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
                0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
                0x90, 0x90, 0xF0, 0x10, 0x10, // 4
                0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
                0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
                0xF0, 0x10, 0x20, 0x40, 0x40, // 7
                0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
                0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
                0xF0, 0x90, 0xF0, 0x90, 0x90, // A
                0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
                0xF0, 0x80, 0x80, 0x80, 0xF0, // C
                0xE0, 0x90, 0x90, 0x90, 0xE0, // D
                0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
                0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };
        for (int i=0; i<64; i++) {
            memory[i] = (byte) hexDigits[i];
        }
    }

    private void displaySplashScreen() {
        screen.clearScreen();

        String[] splash = {
                "0000111111001100000011001111111111001111111100000000000011111100",
                "0011000000001100000011000000110000001100000011000000001100000011",
                "1100000000001100000011000000110000001100000011000000001100000011",
                "1100000000001111111111000000110000001100000011011111100011111100",
                "1100000000001100000011000000110000001111111100000000001100000011",
                "1100000000001100000011000000110000001100000000000000001100000011",
                "0011000000001100000011000000110000001100000000000000001100000011",
                "0000111111001100000011001111111111001100000000000000000011111100"
        };

        int splshCntr = 0;
        for(int y=8; y<24; y+=2) {
            for (int x=0; x< screen.SCREENWIDTH; x++) {
                screen.pxls[x][y] = splash[splshCntr].charAt(x) == '1' ? 1 : 0;
                screen.pxls[x][y+1] = splash[splshCntr].charAt(x) == '1' ? 1 : 0;
            }
            splshCntr++;
        }

        screen.repaint();
    }

    public void loadRom(File rom) throws IOException {
        FileInputStream reader = new FileInputStream(rom);
        int v = 0;
        int c = 0;
        while ((v = reader.read()) != -1) {
            memory[0x200 + c++] = (byte) v;
        }
        System.out.println(c + " bytes read from ROM");
        reader.close();

        romLoaded = true;
    }

    public void start() {
        if (!romLoaded) {
            JOptionPane.showMessageDialog(null, "You must load a ROM before starting the emulator.", "CHIP-8 Emulator", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        running = true;
        if (cleanStart) {
            screen.clearScreen();
            cleanStart = false;
        }
        startContinuousTickCycle();
    }

    private void startContinuousTickCycle() {
        while(running) {
            singleTick();
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        running = false;
        notifyListenersVMPaused();
    }

    public void singleTick() {
        // Decrement timers at 60Hz
        if ((delayTimer != 0 || soundTimer != 0) && (System.currentTimeMillis() >= lastTimerTick + (1000/60))) {
            lastTimerTick = System.currentTimeMillis();
            if (delayTimer != 0)
                delayTimer--;
            if(soundTimer!=0) {
                beep();
                soundTimer--;
            }
        }

        // Tick at 500hz
        int ticksPerMinute = 500;
        if (System.currentTimeMillis() >= lastCpuTick + (1000/ticksPerMinute)) {
            lastCpuTick = System.currentTimeMillis();
            opcode();
            screen.repaint();
        }
        else {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                System.out.println("Emulator tick delay thread interrupted");
                //e.printStackTrace();
            }
        }

        if (!running)
            notifyListenersVMPaused();
    }

    private void opcode() {
        /*
        All opcodes are 2 bytes long
        Since each element of the memory byte array is 1 byte, 1 opcode = 2 elements
        Opcodes are separated into nibbles (4 bytes) and the schema of these nibbles determines which nibbles are the actual opcode and which are the parameters
        MSB =  memory[pc]  = opcode[0] + opcode[1]
        LSB = memory[pc+1] = opcode[2] + opcode[3]
         */

        // Get the opcode fully as well as separated into nibbles
        int msb = (memory[pc] << 8);
        int lsb = memory[pc+1];
        int fullOpcode = (msb & 0xFF00) | (lsb & 0x00FF);

        int[] opcode = new int[4];
        opcode[0] = (memory[pc] & 0xF0) >> 4;
        opcode[1] = memory[pc] & 0x0F;
        opcode[2] = (memory[pc+1] & 0xF0) >> 4;
        opcode[3] = memory[pc+1] & 0x0F;

        System.out.println("Cycle " + ++cycles + "\tPC = " + pc + " Opcode 0x" + Integer.toHexString(fullOpcode).toUpperCase());

        // Advance the program counter
        pc += 2;

        // Act on the opcode
        switch (opcode[0]) {
            case 0x0:
                switch(fullOpcode) {
                    case 0x00E0:
                        // 00E0 - CLS
                        // Clear the display
                        screen.clearScreen();
                        break;
                    case 0x00EE:
                        // 00EE - RET
                        // Return from a subroutine
                        pc = stack[--sp];
                        break;
                    default:
                        // 0NNN - Do nothing with this
                        System.out.println("0NNN - unsupported call to machine code function");
                        break;
                }
                break;

            case 0x1:
                // 1nnn - JP addr
                // Jump to location nnn
                pc = (short) (fullOpcode & 0x0FFF);
                break;

            case 0x2:
                // 2nnn - CALL addr
                // Call subroutine at nnn
                stack[sp++] = pc;
                pc = (short) (fullOpcode & 0x0FFF);
                break;

            case 0x3:
                // 3xkk - SE Vx, byte
                // Skip next instruction if Vx = kk
                if (v[opcode[1]] == (fullOpcode & 0x00FF))
                    pc += 2;
                break;

            case 0x4:
                // 4xkk - SE Vx, byte
                // Skip next instruction if Vx != kk
                if (v[opcode[1]] != (fullOpcode & 0x00FF))
                    pc += 2;
                break;

            case 0x5:
                // 5xy0 - SE Vx, Vy
                // Skip next instruction if Vx = Vy
                if (v[opcode[1]] == v[opcode[2]])
                    pc +=2;
                break;

            case 0x6:
                // 6xkk - LD Vx byte
                // Set Vx = kk
                v[opcode[1]] = fullOpcode & 0x00FF;
                break;

            case 0x7:
                // 7xkk - ADD Vx, byte
                // Set Vx = Vx + kk
                v[opcode[1]] = (v[opcode[1]] + (fullOpcode & 0xFF)) & 0xFF;
                break;

            case 0x8:
                switch (opcode[3]) {
                    case 0x0:
                        // 8xy0 - LD Vx, Vy
                        // Sets Vx = Vy
                        v[opcode[1]] = v[opcode[2]];
                        break;

                    case 0x1:
                        // 8xy1 - OR Vx, Vy
                        // Set Vx = Vx OR Vy
                        v[opcode[1]] = v[opcode[1]] | v[opcode[2]];
                        break;

                    case 0x2:
                        // 8xy2 - AND Vx, Vy
                        // Set Vx = Vx AND Vy
                        v[opcode[1]] = v[opcode[1]] & v[opcode[2]];
                        break;

                    case 0x3:
                        // 8xy3 - XOR Vx, Vy
                        // Sets Vx = Vx XOR Vy
                        v[opcode[1]] = v[opcode[1]] ^ v[opcode[2]];
                        break;

                    case 0x4:
                        // 8xy4 - ADD Vx, Vy
                        // Sets Vx = Vx + Vy, set VF = carry
                        int result = v[opcode[1]] + v[opcode[2]];
                        if (result > 255)
                            v[0xF] = 0x1;
                        else
                            v[0xF] = 0x0;
                        v[opcode[1]] = result & 0xFF;
                        break;

                    case 0x5:
                        // 8xy5 - SUB Vx, Vy
                        // Set Vx = Vx - Vy, set VF = NOT borrow
                        if (v[opcode[1]] > v[opcode[2]])
                            v[0xF] = 0x1;
                        else
                            v[0xF] = 0x0;
                        v[opcode[1]] = (v[opcode[1]] - v[opcode[2]]) & 0xFF;
                        break;

                    case 0x6:
                        // 8xy6 - SHR Vx, Vy
                        // Set Vx = Vy SHR 1
                        v[0xF] = (v[opcode[1]] & 0b1);
                        v[opcode[1]] = (v[opcode[2]] >> 1) & 0xFF;
                        break;

                    case 0x7:
                        // 8xy7 - SUBn Vx, Vy
                        // Set Vx = Vy - Vx, set VF = NOT BORROW
                        if (v[opcode[2]] > v[opcode[1]])
                            v[0xF] = 0x1;
                        else
                            v[0xF] = 0x0;
                        v[opcode[1]] = (v[opcode[2]] - v[opcode[1]]) & 0xFF;
                        break;

                    case 0xE:
                        // 8xyE - SHL Vx, Vy
                        // Set Vx = Vy SHL 1
                        v[0xF] = (v[opcode[2]] >> 7) & 0b1;
                        v[opcode[1]] = (v[opcode[2]] << 1) & 0xFF;
                        break;

                    default:
                        System.out.println("Opcode 0x" + Integer.toHexString(fullOpcode).toUpperCase() + " not recognised/implemented.");
                        break;
                }
                break;

            case 0x9:
                // 9xy0 - SNE Vx, Vy
                // Skip next instruction if Vx != Vy
                if (v[opcode[1]] != v[opcode[2]])
                    pc += 2;
                break;

            case 0xA:
                // Annn - LD I, addr
                // Set I = nnn
                i = (short) (fullOpcode & 0x0FFF);
                break;

            case 0xB:
                // Bnnn - JP V0, addr
                // Jump to location nnn + V0
                pc = (short) (((fullOpcode & 0x0FFF) + v[0x0]) & 0xFFFF);
                break;

            case 0xC:
                // Cxkk - RND Vx, byte
                // Set Vx = random byte AND kk
                int random = (new Random()).nextInt(256);
                v[opcode[1]] = random & (fullOpcode & 0x00FF);
                break;

            case 0xD:
                // Dxyn - DRW Vx, Vy, nibble
                // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision
                int height = fullOpcode & 0x000F;
                byte[] sprite = Arrays.copyOfRange(memory, i, i+height);
                v[0xF] = 0;

                for (int y=0; y<sprite.length; y++) {

                    int drawY = (v[opcode[2]] + y) % Screen.SCREENHEIGHT;

                    for (int x=0; x<8; x++) {

                        int drawX = (v[opcode[1]] + x) % Screen.SCREENWIDTH;
                        int bitToDraw = (sprite[y] >> (7-x)) & 0b1;

                        if (bitToDraw == 1) {
                            if (screen.pxls[drawX][drawY] == 1)
                                v[0xF] = 1;

                            screen.pxls[drawX][drawY] ^= 1;
                        }
                    }

                }
                break;

            case 0xE:
                switch(opcode[3]) {
                    case 0xE:
                        // Ex9E - SKP Vx
                        // Skip next instruction if key with the value of Vx is pressed
                        logKeyCheck(v[opcode[1]]);
                        if (keypad.isKeyPressed(v[opcode[1]]))
                            pc += 2;
                        break;

                    case 0x1:
                        // ExA1 - SKNP Vx
                        // Skip next instruction if key with the value of Vx is not pressed
                        logKeyCheck(v[opcode[1]]);
                        if (!keypad.isKeyPressed(v[opcode[1]]))
                            pc += 2;
                        break;

                    default:
                        System.out.println("Opcode 0x" + Integer.toHexString(fullOpcode).toUpperCase() + " not recognised/implemented.");
                        break;
                }
                break;

            case 0xF:
                switch (fullOpcode & 0x00FF) {
                    case 0x07:
                        // Fx07 - LD Vx, DT
                        // Set Vx = delay timer value
                        v[opcode[1]] = delayTimer;
                        break;
                    case 0x0A:
                        // Fx0A - LD Vx, K
                        // Wait for a key press, store the value of the key in Vx
                        boolean waiting = true;
                        while (waiting) {
                            for (byte k=0; k<16; k++) {
                                if (keypad.isKeyPressed(k)) {
                                    v[opcode[1]] = k;
                                    waiting = false;
                                    break;
                                }
                            }
                        }
                        break;

                    case 0x15:
                        // Fx15 - LD DT, Vx
                        // Set delay timer = Vx
                        delayTimer = (byte) (v[opcode[1]] & 0xFF);
                        break;

                    case 0x18:
                        // Fx18 - LD ST, Vx
                        // Set sound timer = Vx
                        soundTimer = (byte) (v[opcode[1]] & 0xFF);
                        break;

                    case 0x1E:
                        // Fx1E - ADD I, Vx
                        // Set I = I + Vx
                        int result = i + v[opcode[1]];
                        i = (short) (result & 0xFFFF);
                        break;

                    case 0x29:
                        // Fx29 - LD F, Vx
                        // Set I = location of sprite for digit Vx
                        i = (short) ((v[opcode[1]] * 5) & 0xFFFF);
                        break;

                    case 0x33:
                        // Fx33 - LD B, Vx
                        // Store BCD representation of Vx in memory locations I, I+1 and I+2
                        int x = v[opcode[1]];
                        int d = 100;
                        for (int c=0; c<3; c++) {
                            memory[i+c] = (byte) ((x - (x % d)) / d);
                            x = x % d;
                            d /= 10;
                        }
                        break;

                    case 0x55:
                        // Fx55 - LD [I], Vx
                        // Store registers V0 through Vx in memory starting at location I
                        for (int r=0; r<=opcode[1]; r++) {
                            memory[i++ & 0xFFFF] = (byte) v[r];
                        }
                        break;

                    case 0x65:
                        // Fx65 - LD Vx. [I]
                        // Read registers v0 through Vx from memory starting at location I
                        for (int r=0; r<=opcode[1]; r++) {
                            v[r]= memory[i++ & 0xFFFF] & 0x000F;
                        }
                        break;

                    default:
                        System.out.println("Opcode 0x" + Integer.toHexString(fullOpcode).toUpperCase() + " not recognised/implemented.");
                        break;
                }
                break;

            default:
                System.out.println("Opcode 0x" + Integer.toHexString(fullOpcode).toUpperCase() + " not recognised/implemented.");
                break;
        }
    }

    private void beep() {
        Toolkit.getDefaultToolkit().beep();
    }

    private void logKeyCheck(int hex) {
        System.out.println("Key check - " + Integer.toHexString(hex));
    }

    private boolean memoryIsLocked(int memAddress) {
        return memoryLocked[memAddress];
    }

    /* //////////////////////////////////////////////
    Public getters for various VM data
     *///////////////////////////////////////////////

    public int[] getVRegisters() { return v; }

    public short[] getStack() { return stack; }

    public short getPC() { return pc; }

    public byte getDelayTimer() { return delayTimer; }

    public byte getSoundTimer() { return soundTimer; }

    public byte[] getMemory() { return memory; }

    public boolean[] getMemoryLocked() { return memoryLocked; }

    /* //////////////////////////////////////////////
    Variables/methods related to IVMListener and it's usage
     *///////////////////////////////////////////////

    ArrayList<IVMListener> currentVMListeners = new ArrayList<>();

    public void addVMListener(IVMListener listener) {
        currentVMListeners.add(listener);
    }

    private void notifyListenersVMPaused() {
        for(IVMListener listener : currentVMListeners) {
            if (listener == null)
                currentVMListeners.remove(listener);
            else
                listener.reactToVMPause();
        }
    }
}