package com.lazyfaith.chip8emulator;


import javax.swing.*;
import java.awt.*;

public class Screen extends JPanel {
    public static int SCREENWIDTH = 64, SCREENHEIGHT = 32;
    public static int SCALAR = 10;

    public int[][] pxls = new int[SCREENWIDTH][SCREENHEIGHT];

    public Screen() {
        this.setPreferredSize(new Dimension(SCREENWIDTH * SCALAR, SCREENHEIGHT * SCALAR));
        this.repaint(1);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int x=0; x < SCREENWIDTH; x++) {
            for (int y=0; y < SCREENHEIGHT; y++) {
                if (pxls[x][y] == 0x1)
                    g.setColor(Color.WHITE);
                else
                    g.setColor(Color.BLACK);

                g.fillRect(x* SCALAR, y* SCALAR, SCALAR, SCALAR);
            }
        }

        // Useful for checking that none of the CHIP-8 display is being cut off by window elements
        //g.setColor(Color.RED);
        //g.drawRect(0, 0, (SCREENWIDTH * SCALAR) - 1, (SCREENHEIGHT * SCALAR) - 1);

        g.dispose();
    }

    public void clearScreen() {
        for (int x=0; x< SCREENWIDTH; x++) {
            for (int y = 0; y < SCREENHEIGHT; y++) {
                pxls[x][y] = 0;
            }
        }
    }
}