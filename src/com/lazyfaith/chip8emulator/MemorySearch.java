package com.lazyfaith.chip8emulator;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class MemorySearch extends JFrame implements ActionListener, IVMListener {

    @Override
    public void reactToVMPause() {
        new Runnable() {
            @Override
            public void run() {
                updateTable();
            }
        }.run();
    }

    private static class ActionTexts {
        public static final String NEWSEARCH = "New Search";
        public static final String FILTEREQUALTO = "Equal to";
        public static final String FILTERNOTEQUALTO = "Not equal to";
    }

    // Window properties and elements
    private static int WINDOWWIDTH = 300;
    private static int WINDOWHEIGHT = 450;
    JLabel watchInfo;
    JTable table;
    DefaultTableModel tableModel;

    // Reference to current Chip-8 virtual machine instance
    Chip8 vmRef;

    // Memory related variables
    byte[] oldMemoryVals;
    boolean[] memoryToShow;
    int watchCount; // Number of memory addresses still being watched

    public MemorySearch(Chip8 vm) {
        super("Chip-8 Memory Watcher");

        // Set up window
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setSize(WINDOWWIDTH, WINDOWHEIGHT);

        JPanel panelBorderLayout = new JPanel(new BorderLayout());

        JPanel panelNorth = new JPanel(new GridLayout(0,1));
        JButton button = new JButton(ActionTexts.NEWSEARCH);
        button.addActionListener(this);
        panelNorth.add(button);

        watchInfo = new JLabel("Watching n addresses (0 removed)");
        panelNorth.add(watchInfo);

        tableModel = new DefaultTableModel();
        tableModel.addColumn("Address");
        tableModel.addColumn("Value");
        table = new JTable(tableModel);

        JPanel panelSouth = new JPanel();
        button = new JButton(ActionTexts.FILTEREQUALTO);
        button.addActionListener(this);
        panelSouth.add(button);

        button = new JButton(ActionTexts.FILTERNOTEQUALTO);
        button.addActionListener(this);
        panelSouth.add(button);

        panelBorderLayout.add(panelNorth, BorderLayout.NORTH);
        panelBorderLayout.add(new JScrollPane(table), BorderLayout.CENTER);
        panelBorderLayout.add(panelSouth, BorderLayout.SOUTH);
        this.add(panelBorderLayout);

        // Populate data & table
        vmRef = vm;
        newSearch();
    }

    private void newSearch() {
        oldMemoryVals = vmRef.getMemory().clone();
        memoryToShow = new boolean[4096];
        Arrays.fill(memoryToShow, Boolean.TRUE);
        watchCount = 4096;
        updateWatcherInfo(4096);

        // Pause emulator if needed
        boolean wasRunning = vmRef.running;
        if (wasRunning)
            vmRef.stop();

        // Clear current rows
        int numOfRows = tableModel.getRowCount();
        for(int i=0; i<numOfRows; i++)
            tableModel.removeRow(0);

        // Populate data with current memory
        byte[] currentMemory = vmRef.getMemory();
        for(int i=0; i<memoryToShow.length; i++) {
            tableModel.addRow(new Object[]{i, Integer.toHexString(currentMemory[i] & 0xFF)});
        }

        // Restart emulator if it was running
        if (wasRunning)
            vmRef.start();
    }

    private void updateTable() {
        // Get ref of latest memory
        byte[] currentMemory = vmRef.getMemory();

        // Go through all rows
        int i = 0;
        while(i < tableModel.getRowCount()) {
            // If not watching row, remove it from table
            int memAddress = (int) tableModel.getValueAt(i, 0);
            if (memoryToShow[memAddress] == false) {
                tableModel.removeRow(i);
            }
            else {
                // We're watching this row. Update it's "value" data, regardless of correct or not
                tableModel.setValueAt(currentMemory[memAddress], i, 1);
                // Move on to next row
                i++;
            }
        }
    }

    private void filterMemory(boolean shouldBeEqual) {
        // Get ref of latest memory
        byte[] currentMemory = vmRef.getMemory();

        // Filter out memory addresses we don't want to watch any more
        int newWatchCount = watchCount;
        for (int i=0; i<currentMemory.length; i++) {
            if (memoryToShow[i]) {
                if ((currentMemory[i] == oldMemoryVals[i]) != shouldBeEqual) {
                    memoryToShow[i] = false;
                    newWatchCount--;
                }
            }
        }

        updateWatcherInfo(newWatchCount);

        // Copy current memory values into oldMemoryVals for future reference
        oldMemoryVals = currentMemory.clone();

        // Update the table
        updateTable();
    }

    private void updateWatcherInfo(int newWatchCount) {
        watchInfo.setText("Watching " + newWatchCount + " addresses (" + (watchCount-newWatchCount) + " removed)");
        watchCount = newWatchCount;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionText = ((JButton) e.getSource()).getText();
        switch (actionText) {
            case ActionTexts.NEWSEARCH: newSearch();
                break;
            case ActionTexts.FILTEREQUALTO: filterMemory(true);
                break;
            case ActionTexts.FILTERNOTEQUALTO: filterMemory(false);
                break;
        }
    }
}
