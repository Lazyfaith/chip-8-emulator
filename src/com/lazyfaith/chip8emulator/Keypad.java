package com.lazyfaith.chip8emulator;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keypad implements KeyListener {

    private boolean[] keyStatuses = new boolean[16];

    public boolean isKeyPressed(int keyVal) {
        return keyStatuses[keyVal];
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Not needed
    }

    @Override
    public void keyPressed(KeyEvent e) {
        toggleKey(e, true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        toggleKey(e, false);
    }

    private void toggleKey(KeyEvent key, boolean val) {
        //System.out.println("Key " + key.getKeyChar() + "\t" + val);

        switch (key.getKeyCode()) {
            case KeyEvent.VK_0:
                keyStatuses[0x0] = val;
                break;
            case KeyEvent.VK_1:
                keyStatuses[0x1] = val;
                break;
            case KeyEvent.VK_2:
                keyStatuses[0x2] = val;
                break;
            case KeyEvent.VK_3:
                keyStatuses[0x3] = val;
                break;
            case KeyEvent.VK_4:
                keyStatuses[0x4] = val;
                break;
            case KeyEvent.VK_5:
                keyStatuses[0x5] = val;
                break;
            case KeyEvent.VK_6:
                keyStatuses[0x6] = val;
                break;
            case KeyEvent.VK_7:
                keyStatuses[0x7] = val;
                break;
            case KeyEvent.VK_8:
                keyStatuses[0x8] = val;
                break;
            case KeyEvent.VK_9:
                keyStatuses[0x9] = val;
                break;
            case KeyEvent.VK_A:
                keyStatuses[0xA] = val;
                break;
            case KeyEvent.VK_B:
                keyStatuses[0xB] = val;
                break;
            case KeyEvent.VK_C:
                keyStatuses[0xC] = val;
                break;
            case KeyEvent.VK_D:
                keyStatuses[0xD] = val;
                break;
            case KeyEvent.VK_E:
                keyStatuses[0xE] = val;
                break;
            case KeyEvent.VK_F:
                keyStatuses[0xF] = val;
                break;
            default:
                // Key we don't care about
                break;
        }
    }
}
