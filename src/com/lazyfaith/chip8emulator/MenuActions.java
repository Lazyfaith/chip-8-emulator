package com.lazyfaith.chip8emulator;

public class MenuActions {

    public static final String LOADROM = "Load ROM...";
    public static final String EXIT = "Exit";

    public static final String START = "Start / Resume";
    public static final String PAUSESTEP = "Pause / Step";
    public static final String HARDRESET = "Hard Reset";

    public static final String DEBUGWINDOWS = "Debug Windows...";
    public static final String MEMORYSEARCH = "Memory Search...";
}
