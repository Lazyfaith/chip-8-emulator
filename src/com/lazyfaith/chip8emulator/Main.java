package com.lazyfaith.chip8emulator;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

public class Main extends JFrame implements ActionListener, Thread.UncaughtExceptionHandler {

    static String WINDOWTITLE = "CHIP-8 Emulator";

    static Chip8 vm;
    static Thread emulationThread;

    static MemorySearch memorySearch;

    public Main(String[] args) {
        super(WINDOWTITLE);

        vm = new Chip8();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(vm.screen);
        this.setSize(Screen.SCREENWIDTH * Screen.SCALAR, Screen.SCREENHEIGHT * Screen.SCALAR);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.addKeyListener(vm.keypad);

        // Menubar at top of window
        JMenuBar menuBar = new JMenuBar();
        JMenuItem menuItem = null;

        // File menu
        JMenu fileMenu = new JMenu("File");

        menuItem = new JMenuItem(MenuActions.LOADROM);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        fileMenu.addSeparator();

        menuItem = new JMenuItem(MenuActions.EXIT);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        // Emulation menu
        JMenu emulationMenu = new JMenu("Emulation");

        menuItem = new JMenuItem(MenuActions.START);
        menuItem.addActionListener(this);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        emulationMenu.add(menuItem);

        menuItem = new JMenuItem(MenuActions.PAUSESTEP);
        menuItem.addActionListener(this);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
        emulationMenu.add(menuItem);

        emulationMenu.addSeparator();

        menuItem = new JMenuItem(MenuActions.HARDRESET);
        menuItem.addActionListener(this);
        emulationMenu.add(menuItem);

        // Window menu
        JMenu windowMenu = new JMenu("Window");

        menuItem = new JMenuItem(MenuActions.DEBUGWINDOWS);
        menuItem.addActionListener(this);
        windowMenu.add(menuItem);

        menuItem = new JMenuItem(MenuActions.MEMORYSEARCH);
        menuItem.addActionListener(this);
        windowMenu.add(menuItem);

        // Assemble the menu bar and add to JFrame
        menuBar.add(fileMenu);
        menuBar.add(emulationMenu);
        menuBar.add(windowMenu);
        this.setJMenuBar(menuBar);

        this.pack();
        this.setVisible(true);

        // Process any command line arguments
    }
    public static void main(String[] args){
        Main mainWindow = new Main(args);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = ((JMenuItem) e.getSource()).getText();

        switch(action) {
            case MenuActions.LOADROM:
                menuLoadRoam();
                break;
            case MenuActions.EXIT:
                System.exit(0);
                break;
            case MenuActions.START:
                menuStartResumeEmulator();
                break;
            case MenuActions.PAUSESTEP:
                menuPauseStepEmulator();
                break;
            case MenuActions.HARDRESET:
                menuHardResetEmulator();
                break;
            case MenuActions.DEBUGWINDOWS:
                menuDebugWindows();
                break;
            case MenuActions.MEMORYSEARCH:
                menuMemorySearch();
        }
    }

    private void menuLoadRoam() {
        menuHardResetEmulator();

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("CHIP-8 ROMs (.ch8, .bin)", "ch8", "bin"));
        int result = fileChooser.showOpenDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {
            File rom = fileChooser.getSelectedFile();

            if (!rom.canRead()) {
                JOptionPane.showMessageDialog(null, "File could not be read.", "CHIP-8 Emulator", JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                vm.loadRom(rom);
            }
            catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Chosen ROM is too large to be a CHIP-8 ROM.", "CHIP-8 Emulator", JOptionPane.ERROR_MESSAGE);
                return;
            } catch (IOException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error whilst reading ROM file:\n\n" + e.getMessage(), "CHIP-8 Emulator", JOptionPane.ERROR_MESSAGE);
                return;
            }

            this.setTitle(WINDOWTITLE + " - " + rom.getName());
            JOptionPane.showMessageDialog(null, "ROM \"" + rom.getName() + "\" loaded into memory.", "CHIP-8 Emulator", JOptionPane.INFORMATION_MESSAGE);
            vm.screen.clearScreen();
        }
    }

    private void menuStartResumeEmulator() {
        if (!vm.isRunning()) {
            startVM();
        }
    }

    private void startVM() {
        if (emulationThread != null && emulationThread.isAlive())
            emulationThread.interrupt();

        emulationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                vm.start();
            }
        });
        emulationThread.start();
        emulationThread.setUncaughtExceptionHandler(this);
    }

    private void menuPauseStepEmulator() {
        if (!vm.romLoaded)
            return;

        if (vm.isRunning())
            vm.stop();
        else
            vm.singleTick();
    }

    private void menuHardResetEmulator() {
        if (vm.isRunning())
            vm.stop();

        if (emulationThread != null && emulationThread.isAlive())
            emulationThread.interrupt();

        vm.hardReset();
        this.setTitle(WINDOWTITLE);
    }

    private void menuDebugWindows() {

    }

    private void menuMemorySearch() {
        // Ensure instance of the Memory Search window exists
        if (memorySearch == null) {
            memorySearch = new MemorySearch(vm);
            vm.addVMListener(memorySearch);
        }
        // Now show window / bring to front
        if (!memorySearch.isVisible())
            memorySearch.setVisible(true);

        memorySearch.toFront();
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(null, "Emulation has stopped due to an uncaught error whilst running the ROM.", "CHIP-8 Emulator", JOptionPane.ERROR_MESSAGE);
    }
}