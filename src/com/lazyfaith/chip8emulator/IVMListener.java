package com.lazyfaith.chip8emulator;

public interface IVMListener {
    public void reactToVMPause();
}
